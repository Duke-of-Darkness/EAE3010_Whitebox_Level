﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public Transform target;
    public Vector3 distance = new Vector3(0f, 2f, -3f);
    public float positionDampen = 2f;
    public float rotationDampen = 2f;

    private Transform thisTransform;

	// Use this for initialization
	void Start () {
        thisTransform = GetComponent<Transform>();
        
	}
	
	// Update is called once per frame
	void LateUpdate () {

        if (target == null)
            return;

        Vector3 wantedPosition = target.position + (target.rotation * distance);
        Vector3 currentPosition = Vector3.Lerp(thisTransform.position, wantedPosition, positionDampen * Time.deltaTime);

        thisTransform.position = currentPosition;
        Quaternion wantedRotation = Quaternion.LookRotation(target.position - thisTransform.position, target.up);

        thisTransform.rotation = wantedRotation;
	}
}
